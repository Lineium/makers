//
//  EventViewController.swift
//  makers
//
//  Created by Diego Quiroz García on 02/04/17.
//  Copyright © 2017 Lineium. All rights reserved.
//

import UIKit
import Material

class EventNavigationViewController: NavigationController {

    open override func prepare() {
        super.prepare()
        guard let v = navigationBar as? NavigationBar else {
            return
        }
        
        v.depthPreset = .none
        v.dividerColor = Color.grey.lighten3
    }

}

class EventViewController: UIViewController {
    
    var eventID: PFObject?
    
    fileprivate var backBtn: IconButton!
    fileprivate var starBtn: IconButton!

    @IBOutlet weak var eventDescription: UILabel!
    @IBOutlet weak var eventImage: UIImageView!
    
    @IBOutlet weak var username: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = Color.grey.lighten5
        
        if let object = eventID {
            prepareBackBtn()
            prepareStarBtn()
            
            if let Description = object["descripcion"] as? String {
                eventDescription.text = Description
            }
            
            if let eName = object["nombre"] as? String, let eCategory = object["category"] as? String {
                prepareNavigationItem(name: eName, cat: eCategory)
            }
            
            let imageFile = object["background"] as? PFFile
            imageFile?.getDataInBackground({ (imageData, error) in
                if error == nil {
                    if let imageData = imageData {
                        let Image = UIImage(data:imageData)
                        self.eventImage.image = Image!
                    }
                } else {
                    print(error as Any)
                }
                
            })


        }
        
        username.text = PFUser.current()?.username
        
        
    }
    
    func back() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TabView") as! UITabBarController
        self.present(vc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension EventViewController {
    fileprivate func prepareBackBtn() {
        backBtn = IconButton(image: Icon.cm.arrowBack)
        backBtn.tintColor = Color.pink.base
        backBtn.addTarget(self, action: #selector(back), for: UIControlEvents.touchUpInside)
    }
    
    fileprivate func prepareStarBtn() {
        starBtn = IconButton(image: Icon.cm.star)
        starBtn.tintColor = Color.pink.base
    }
    
    fileprivate func prepareNavigationItem(name: String, cat: String) {
        navigationItem.title = name
        navigationItem.detail = cat
        
        navigationItem.leftViews = [backBtn]
        navigationItem.rightViews = [starBtn]
    }
}

extension EventViewController {
    
}
