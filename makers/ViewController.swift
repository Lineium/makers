//
//  ViewController.swift
//  makers
//
//  Created by Diego Quiroz García on 30/09/16.
//  Copyright © 2016 Lineium. All rights reserved.
//

import UIKit

class ViewController: UIViewController, BWWalkthroughViewControllerDelegate {
    
    var walkthrough:BWWalkthroughViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        validateUser()
        
    }
    
    
    
    func validateUser() {
        if (PFUser.current() == nil) {
            
            showWalkthrough()
        } else {
            
            self.performSegue(withIdentifier: "alreadyLogedIn", sender: self)

        }
    }
    
//    func firstLaunch() {
//        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
//        if launchedBefore  {
//            print("Not first launch.")
//            //            let main = UIStoryboard(name: "Main", bundle: nil)
//            //            let CursosViewController = main.instantiateViewControllerWithIdentifier("cursos")
//            //
//            //
//            //            self.presentViewController(UserLogViewController(), animated: false, completion: nil)
//            
//            showWalkthrough()
//        }
//        else {
//            print("First launch, setting NSUserDefault.")
//            UserDefaults.standard.set(true, forKey: "launchedBefore")
//            
//            showWalkthrough()
//        }
//    }
    
    
    
    func showWalkthrough() {
        // Get view controllers and build the walkthrough
        let stb = UIStoryboard(name: "Walkthrough", bundle: nil)
        let walkthrough = stb.instantiateViewController(withIdentifier: "Master") as! BWWalkthroughViewController
        let page_one = stb.instantiateViewController(withIdentifier: "page1")
        let page_two = stb.instantiateViewController(withIdentifier: "page2")
        let page_three = stb.instantiateViewController(withIdentifier: "page3")

        
        // Attach the pages to the master
        walkthrough.delegate = self
        walkthrough.add(viewController:page_one)
        walkthrough.add(viewController:page_two)
        walkthrough.add(viewController:page_three)

        
        self.present(walkthrough, animated: true, completion: nil)
    }
    

    
    //    @IBAction func logOut(sender: UIButton) {
    //
    //        PFUser.logOut()
    //        validateUser()
    //    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

//extension ViewController {
//    func walkthroughPageDidChange(pageNumber:Int) {
//        if (self.walkthrough.numberOfPages) == pageNumber{
//            
//            walkthrough.startB
//            
//        } else {
//            self.startBtn.isHidden = true
//        }
//    }
//}


class UserLogViewController: UIViewController, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        validateUser()
    }
    
    
    func validateUser() {
        if (PFUser.current() == nil) {
            
            presentLogInView()
        } else {
            self.performSegue(withIdentifier: "alreadyLogedIn2", sender: self)
            
            
            
        }
    }
    
    // ALERT FUNCTION
    
    func alert(message: String, title: String = "", autoDismiss: Bool) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        self.present(alertController, animated: true, completion: nil)
        
        if (autoDismiss) {
            let when = DispatchTime.now() + 2 // change 2 to desired number of seconds
            DispatchQueue.main.asyncAfter(deadline: when) {
                // Your code with delay
                self.dismiss(animated: true, completion: nil)
//                self.performSegue(withIdentifier: "alreadyLogedIn2", sender: self)
            }
        } else {
            let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(OKAction)

        }
        
    }
    
    
    func log(_ logInController: PFLogInViewController, didLogIn user: PFUser) {
        alert(message: "Welcome to Makers", title: "Your-re logged in", autoDismiss: true)
    }
    
    func signUpViewController(_ signUpController: PFSignUpViewController, didSignUp user: PFUser) {
        validateUser()
    }
    

    


    func presentLogInView() {
        let loginViewController = LogInViewController()
        loginViewController.delegate = self
        loginViewController.fields = [.usernameAndPassword,.logInButton, .passwordForgotten, .signUpButton]
//        loginViewController.emailAsUsername = true
//        loginViewController.signUpController?.emailAsUsername = true
        loginViewController.signUpController?.delegate = self
        self.present(loginViewController, animated: false, completion: nil)
    }
    
}




//class LogInViewController: UIViewController, UITextFieldDelegate {
//    
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        // Do any additional setup after loading the view.
//        
//
//        
//    }
//    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        
//        validateUser()
//        
//    }
//    
//    
//    
//    // FACEBOOK LOGIN
//    
//    let permissions = ["public_profile", "email", "user_friends"]
//    
//    @IBAction func fbLoginClick(sender: AnyObject) {
//        
//        FBLogIn()
//    }
//    
//    func validateUser() {
//        if (PFUser.current() == nil) {
//            print("No hay usuario")
//        } else {
//            
//            self.performSegue(withIdentifier: "alreadyLogedIn", sender: self)
//
//        }
//    }
//    
//    func FBLogIn() {
//        
//        PFFacebookUtils.logInInBackground(withReadPermissions: permissions) { (user: PFUser?, error: Error?) in
//            if let user = user {
//                if user.isNew {
//                    print("User signed up and logged in through Facebook!")
//                } else {
//                    print("User logged in through Facebook!")
//                }
//            } else {
//                print("Uh oh. The user cancelled the Facebook login.")
//            }
//        }
//
//    }
//    
//    
//    // Keyboard return
//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.view.endEditing(true)
//        return false
//        
//    }
//    
//    
//    
//    
//    
//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//        // Dispose of any resources that can be recreated.
//    }
//    
//    
//    /*
//     // MARK: - Navigation
//     
//     // In a storyboard-based application, you will often want to do a little preparation before navigation
//     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//     // Get the new view controller using segue.destinationViewController.
//     // Pass the selected object to the new view controller.
//     }
//     */
//    
//}
