//
//  ClassCollectionViewCell.swift
//  makers
//
//  Created by Diego Quiroz García on 02/10/16.
//  Copyright © 2016 Lineium. All rights reserved.
//

import UIKit

class ClassCollectionViewCell: PFCollectionViewCell {
    
    @IBOutlet weak var classBackground: PFImageView!
    @IBOutlet weak var className: UILabel!
    @IBOutlet weak var eventDate: UILabel!
    @IBOutlet weak var eventDescription: UILabel!
    
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var baseView: UIView!
    
    override func layoutSubviews() {
        //        baseView.layer.cornerRadius = baseView.bounds.width / 5.0
        //        baseView.layer.masksToBounds = true
        
        baseView.layer.cornerRadius = 5.0
        baseView.clipsToBounds = true
        
        
        shadowView.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        shadowView.layer.shadowOpacity = 0.7
        shadowView.layer.shadowRadius = 5.0
        
        
        
        
    }
    
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}


class CourseTableViewCell: PFTableViewCell {
    
    
    @IBOutlet weak var courseBackground: PFImageView!
    @IBOutlet weak var courseIcn: PFImageView!
    @IBOutlet weak var courseName: UILabel!
    
    @IBOutlet weak var baseView: UIView!
    @IBOutlet weak var shadowView: UIView!
    
    override func layoutSubviews() {
        //        baseView.layer.cornerRadius = baseView.bounds.width / 5.0
        //        baseView.layer.masksToBounds = true
        
        baseView.layer.cornerRadius = 5.0
        baseView.clipsToBounds = true
        
        
        shadowView.layer.shadowColor = UIColor.black.withAlphaComponent(0.3).cgColor
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
        shadowView.layer.shadowOpacity = 1.0
        shadowView.layer.shadowRadius = 8.0
        
        
        
        
    }
    
    
}

