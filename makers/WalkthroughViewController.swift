//
//  WalkthroughViewController.swift
//  makers
//
//  Created by Diego Quiroz García on 30/09/16.
//  Copyright © 2016 Lineium. All rights reserved.
//

import UIKit

class WalkthroughViewController: BWWalkthroughViewController {

    var vc = ViewController()
    var walkthrough:BWWalkthroughViewController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */

}

class CustomPageWalkthrough: BWWalkthroughPageViewController {
    
    @IBOutlet weak var startBtn: UIButton!
    
    override func viewDidAppear(_ animated: Bool) {

        let maskLayer = CAShapeLayer()
        maskLayer.frame = maskLayer.bounds
        maskLayer.path = UIBezierPath(roundedRect: startBtn.bounds, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: 10, height: 10)).cgPath
        startBtn.layer.mask = maskLayer


    }
    
    @IBAction func start(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "InitialView")
        
        self.present(vc, animated: true, completion: nil)
    }
}
