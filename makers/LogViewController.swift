//
//  LogInViewController.swift
//  makers
//
//  Created by Diego Quiroz García on 18/10/16.
//  Copyright © 2016 Lineium. All rights reserved.
//

import UIKit

class LogInViewController: PFLogInViewController {
    
    
    var backgroundImage : UIImageView!;
    
    var viewsToAnimate: [UIView?]!;
    
    var viewsFinalYPosition : [CGFloat]!;

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.

        
//        if (PFUser.current() !== nil) {
//            let appView = AppViewController()
//            self.present(appView, animated: false, completion: nil)
//        } else {
//        }
        
        // set our custom background image
        backgroundImage = UIImageView(image: UIImage(named: "bgGR2"))
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        logInView!.insertSubview(backgroundImage, at: 0)
        
        // remove the parse Logo
        let logo = UILabel()
        logo.text = "makers"
        logo.textColor = UIColor.white
        logo.font = UIFont(name: "Milkshake", size: 70)
        
        logInView?.logo = logo
        
        // set forgotten password button to white
        logInView?.passwordForgottenButton?.setTitleColor(UIColor.white, for: .normal)
        
        // make the background of the login button pop more
        logInView?.logInButton?.setBackgroundImage(nil, for: .normal)
        logInView?.logInButton?.backgroundColor = UIColor(red: 242/255, green: 38/255, blue: 19/255, alpha: 1)
        // make the buttons classier
//        customizeButton(button: logInView?.facebookButton)
        //        customizeButton(logInView?.twitterButton!)
        customizeButton(button: logInView?.logInButton)
        logInView?.logInButton?.layer.borderWidth = 0
        customizeButton(button: logInView?.signUpButton!)
        
        // create an array of all the views we want to animate in when we launch
        // the screen
        viewsToAnimate = [self.logInView?.usernameField, self.logInView?.passwordField, self.logInView?.logInButton, self.logInView?.passwordForgottenButton, self.logInView?.facebookButton, self.logInView?.twitterButton, self.logInView?.signUpButton, self.logInView?.logo]
        
        // use our custom SignUpViewController
        self.signUpController = SignUpViewController()

    }
    
    func customizeButton(button: UIButton!) {
        button.setBackgroundImage(nil, for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.white.cgColor
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // stretch background image to fill screen
        backgroundImage.frame = CGRect( x: 0,  y: 0,  width: logInView!.frame.width,  height: logInView!.frame.height)
        
        // position logo at top with larger frame
        logInView!.logo!.sizeToFit()
        //        let logoFrame = logInView!.logo!.frame
        //        logInView!.logo!.frame = CGRectMake(logoFrame.origin.x, logInView!.usernameField!.frame.origin.y - logoFrame.height - 16, logInView!.frame.width,  logoFrame.height)
        
        // We to position all the views off the bottom of the screen
        // and then make them rise back to where they should be
        // so we track their final position in an array
        // but change their frame so they are shifted downwards off the screen
        viewsFinalYPosition = [CGFloat]();
        //        for viewToAnimate in viewsToAnimate {
        //            let currentFrame = viewToAnimate!.frame
        //            viewsFinalYPosition.append(currentFrame.origin.y)
        //            viewToAnimate.frame = CGRectMake(currentFrame.origin.x, self.view.frame.height + currentFrame.origin.y, currentFrame.width, currentFrame.height)
        //        }
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Now we'll animate all our views back into view
        // so they are no longer transparent (alpha = 1)
        // and, using the final position we stored, we'll
        // reset them to where they should be
        if viewsFinalYPosition.count == self.viewsToAnimate.count {
            UIView.animate(withDuration: 1, delay: 0.0, options: .curveEaseInOut,  animations: { () -> Void in
                for viewToAnimate in self.viewsToAnimate {
                    //viewToAnimate.alpha = 1
                    let currentFrame = viewToAnimate?.frame
                    viewToAnimate?.frame = CGRect(x: (currentFrame?.origin.x)!, y: self.viewsFinalYPosition.remove(at: 0), width: (currentFrame?.width)!, height: (currentFrame?.height)!)
                }
                }, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class SignUpViewController: PFSignUpViewController {
    
    var backgroundImage : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        // set our custom background image
        backgroundImage = UIImageView(image: UIImage(named: "bgGR2"))
        backgroundImage.contentMode = UIViewContentMode.scaleAspectFill
        signUpView!.insertSubview(backgroundImage, at: 0)
        
        // remove the parse Logo
        let logo = UILabel()
        logo.text = "makers"
        logo.textColor = UIColor.white
        logo.font = UIFont(name: "Milkshake", size: 70)
        logo.shadowColor = UIColor.lightGray
        signUpView?.logo = logo
        
        // make the background of the sign up button pop more
        signUpView?.signUpButton!.setBackgroundImage(nil, for: .normal)
        signUpView?.signUpButton!.backgroundColor = UIColor(red: 52/255, green: 191/255, blue: 73/255, alpha: 1)
        
        // change dismiss button to say 'Already signed up?'
        signUpView?.dismissButton!.setTitle("Already signed up?", for: .normal)
        signUpView?.dismissButton!.setImage(nil, for: .normal)
        
        customizeButton(button: signUpView?.signUpButton)
        
        // modify the present tranisition to be a flip instead
        self.modalTransitionStyle = UIModalTransitionStyle.flipHorizontal
    }
    
    func customizeButton(button: UIButton!) {
        button.setBackgroundImage(nil, for: .normal)
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 0
        button.layer.borderColor = UIColor.white.cgColor
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // stretch background image to fill screen
        backgroundImage.frame = CGRect(x: 0, y: 0, width: (signUpView?.frame.width)!, height: (signUpView?.frame.height)!)
        
        // position logo at top with larger frame
        signUpView!.logo!.sizeToFit()
        let logoFrame = signUpView!.logo!.frame
        signUpView!.logo!.frame = CGRect(x: logoFrame.origin.x, y: signUpView!.usernameField!.frame.origin.y - logoFrame.height - 16, width: signUpView!.frame.width, height: logoFrame.height)
//        signUpView!.logo!.frame = CGRect(logoFrame.origin.x, signUpView!.usernameField!.frame.origin.y - logoFrame.height - 16, signUpView!.frame.width,  logoFrame.height)
        
        // re-layout out dismiss button to be below sign
        let dismissButtonFrame = signUpView!.dismissButton!.frame
        signUpView?.dismissButton!.frame = CGRect(x: 0, y: signUpView!.signUpButton!.frame.origin.y + signUpView!.signUpButton!.frame.height + 16.0, width: signUpView!.frame.width, height: dismissButtonFrame.height)
//        signUpView?.dismissButton!.frame = CGRect(0, signUpView!.signUpButton!.frame.origin.y + signUpView!.signUpButton!.frame.height + 16.0,  signUpView!.frame.width,  dismissButtonFrame.height)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
