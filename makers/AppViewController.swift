//
//  AppViewController.swift
//  makers
//
//  Created by Diego Quiroz García on 02/10/16.
//  Copyright © 2016 Lineium. All rights reserved.
//

import UIKit

class AppViewController: PFQueryCollectionViewController {
    
    fileprivate var buttons = [Button]()
    fileprivate var tabBar: TabBar!
    
    var category = "Boda"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        
        prepareButtons()
        prepareTabBar()
        
        let userId = (PFUser.current()?.objectId)! as String
        print(userId)
    }

    
    //Logout temporal
    
    @IBAction func logOut(_ sender: Any) {
        PFUser.logOut()
    }
    
    @IBAction func logOut2(_ sender: Any) {
        PFUser.logOut()
    }
    
    
    // No estoy seguro para que es esto, pero sin el no sirve
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Configure the PFQueryCollectionView
        self.parseClassName = "Eventos"
        self.pullToRefreshEnabled = false
        self.paginationEnabled = true
//        self.category = "wedding"
        //        self.objectsPerPage = 15
    }
    

    
    func Wedding(sender: UIButton!) {

        self.category = "Boda"
        self.loadObjects()
        print(category)
    }
    
    

    
    func Party(sender: UIButton!) {

        self.category = "Fiesta"
        self.loadObjects()
        print(category)
    }
    

    
    func Kids(sender: UIButton!) {

        self.category = "Piñata"
        self.loadObjects()
        print(category)
    }
    
    func XV(sender: UIButton!) {
        
        self.category = "xv"
        self.loadObjects()
        print(category)
    }
    
    
    
//    func setupHorizontalBar() {
//        let horizontalBarView = UIView()
//        horizontalBarView.backgroundColor = UIColor.white
//        horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
//        addSubview(horizontalBarView)
//        
//        
//        horizontalBarLeftAnchorConstraint = horizontalBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
//        horizontalBarLeftAnchorConstraint?.isActive = true
//        
//        horizontalBarView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
//        horizontalBarView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/4).isActive = true
//        horizontalBarView.heightAnchor.constraint(equalToConstant: 4).isActive = true
//    }
    
    //============================
    // Setup Table View
    // ===========================

    //Aquí se mandan a llamar los datos de la clase Eventos y sus filtros para solo traer los eventos requeridos.
    func baseQuery() -> PFQuery<PFObject> {

        
        let query = PFQuery(className: "Eventos")
        query.includeKey("createdBy")
//        query.whereKey("createdBy", equalTo: PFUser.current() as Any)
        query.whereKey("category", equalTo: category)
        query.order(byAscending: "createdAt")


        
        return query
    }
    
    override func queryForCollection() -> PFQuery<PFObject> {
        return self.baseQuery()
    }
    
    
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    

    
    //Cell
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, object: PFObject?) -> PFCollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventosCell", for: indexPath) as! ClassCollectionViewCell
        
        let Name = object?["nombre"] as? String
        cell.className.text = Name
        
        let Description = object?["descripcion"] as? String
        cell.eventDescription.text = Description
        
        let Date = object?["fecha"] as? String
        cell.eventDate.text = Date
        
        let imageFile = object?["background"] as? PFFile
        imageFile?.getDataInBackground({ (imageData, error) in
            if error == nil {
                if let imageData = imageData {
                    let Image = UIImage(data:imageData)
                    cell.classBackground.image = Image!
                }
            } else {
                print(error as Any)
            }
            
        })
        
        
        return cell
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "eventSelected" {
            let predestinationVC = segue.destination as? EventNavigationViewController
             let destinationVC = predestinationVC?.viewControllers.first as? EventViewController
            
            let cell = sender as? ClassCollectionViewCell
            let indexPath = collectionView?.indexPath(for: cell!)
            // use indexPath
            let object = self.object(at: indexPath)
            destinationVC?.eventID = object
            
            
            
        }
    }
}

extension AppViewController {
    fileprivate func prepareButtons() {
        let btn1 = FlatButton(title: nil, titleColor: Color.pink.base)
        btn1.pulseAnimation = .point
        buttons.append(btn1)
        btn1.addTarget(self, action: #selector(Wedding), for: .touchUpInside)
        let weddingImage = UIImage(named: "Wedding")
        let tintedImage = weddingImage?.withRenderingMode(.alwaysTemplate)
        btn1.setImage(tintedImage, for: .normal)
        btn1.tintColor = Color.pink.base
        
        let btn2 = FlatButton(title: "Party", titleColor: Color.pink.base)
        btn2.pulseAnimation = .point
        buttons.append(btn2)
        btn2.addTarget(self, action: #selector(Party), for: .touchUpInside)
        
        let btn3 = FlatButton(title: "Kids", titleColor: Color.pink.base)
        btn3.pulseAnimation = .point
        buttons.append(btn3)
        btn3.addTarget(self, action: #selector(Kids), for: .touchUpInside)
        
        let btn4 = FlatButton(title: "XVs", titleColor: Color.pink.base)
        btn4.pulseAnimation = .point
        buttons.append(btn4)
        btn4.addTarget(self, action: #selector(Kids), for: .touchUpInside)

    }
    
    fileprivate func prepareTabBar() {
        tabBar = TabBar()
        tabBar.delegate = self
        
        
        
        tabBar.dividerColor = Color.grey.lighten3
        tabBar.dividerAlignment = .bottom
        
        tabBar.lineColor = Color.pink.base
        tabBar.lineAlignment = .bottom
        
        tabBar.backgroundColor = Color.white
        tabBar.buttons = buttons
        
        view.layout(tabBar).horizontally().top(20)
    }

    
}
extension AppViewController: TabBarDelegate {
    func tabBar(tabBar: TabBar, willSelect button: UIButton) {
    }
    
    func tabBar(tabBar: TabBar, didSelect button: UIButton) {
        
    }
}



class AppViewController2: PFQueryTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print("Should Work Fine")
        UIApplication.shared.statusBarStyle = .lightContent
        
        for parent in self.navigationController!.navigationBar.subviews {
            for childView in parent.subviews {
                if(childView is UIImageView) {
                    childView.removeFromSuperview()
                }
            }
        }

    }

    
    
    //============================
    // Setup Table View
    // ===========================
    
    func baseQuery() -> PFQuery<PFObject> {
        let query = PFQuery(className: "Cursos") // Replace with the class name for the Parse data you're interested in.
        query.whereKey("premium", equalTo: false) // Just like with a regular PFQuery you can filter and sort.
        query.order(byAscending: "createdAt")
        return query
    }
    
    override func queryForTable() -> PFQuery<PFObject> {
        return self.baseQuery().fromLocalDatastore()
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath, object: PFObject?) -> PFTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "courseCell", for: indexPath) as! CourseTableViewCell
        
        
        
        let Name = object?["name"] as? String
        
        cell.courseName.text = Name!
        
        
        
        let imageFile = object?["background"] as? PFFile
        imageFile?.getDataInBackground({ (imageData, error) in
            if error == nil {
                if let imageData = imageData {
                    let Image = UIImage(data:imageData)
                    cell.courseBackground.image = Image!
                }
            } else {
                print(error as Any)
            }
            
        })
        
        
        
        let IconFile = object?["icon"] as? PFFile
        IconFile?.getDataInBackground({ (imageData, error) in
            if error == nil {
                if let imageData = imageData {
                    let Image = UIImage(data:imageData)
                    cell.courseIcn.image = Image!
                }
            } else {
                print(error as Any)
            }
            
        })
        
        
        
        
        
        return cell
        
    }

    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class profileViewController: UIViewController {
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBAction func logOut(_ sender: Any) {
        
        PFUser.logOut()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let Login = storyboard.instantiateViewController(withIdentifier: "ViewC")
        self.present(Login, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        let maskLayer = CAShapeLayer()
        maskLayer.frame = maskLayer.bounds
        maskLayer.path = UIBezierPath(roundedRect: userImage.bounds, byRoundingCorners: UIRectCorner.allCorners, cornerRadii: CGSize(width: 100, height: 100)).cgPath
        userImage.layer.mask = maskLayer
        

        username.text = PFUser.current()?.username
    }
}
